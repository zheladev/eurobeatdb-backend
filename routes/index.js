var express = require('express');
var router = express.Router();
const models = require('./../database/models');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({health: 'ok'})
  
});

module.exports = router;
