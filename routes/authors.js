var express = require('express');
var router = express.Router();
const authorController = require('./../controller/author');
const authorModel = require('./../database/models').Author;
//TODO
/* GET home page. */
router.get('/', authorController.getAllAuthors);

router.get('/:authorId', authorController.getAuthorById);

router.get('/:authorId/songs', function(req, res, next) {
  //todo
});

router.post('/', authorController.createAuthor);

module.exports = router;