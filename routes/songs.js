var express = require('express');
var router = express.Router();
const models = require('./../database/models');
const Op = models.Sequelize.Op;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({ title: 'Songs' });
});

module.exports = router;