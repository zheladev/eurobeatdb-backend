'use strict';
module.exports = (sequelize, DataTypes) => {
    const Author = sequelize.define('Author', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        realName: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        }
    });
    Author.associate = (models) => {
        Author.hasMany(models.Song);
    };
    return Author;
  };