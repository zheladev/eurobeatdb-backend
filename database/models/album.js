'use strict';
module.exports = (sequelize, DataTypes) => {
    const Album = sequelize.define('Album', {
        id: { 
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        record_company: { //change to rel
            type: DataTypes.STRING,
            allowNull: true
        },
        release: {
            type: DataTypes.INTEGER,
            allowNull: true
        }
    });
    Album.associate = (models) => {
        Album.belongsToMany(models.Song, {through: 'AlbumSongs'});
    }
    return Album;
  };