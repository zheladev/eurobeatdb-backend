'use strict';
module.exports = (sequelize, DataTypes) => {
    const Song = sequelize.define('Song', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        length: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    });
    Song.associate = (models) => {
        Song.belongsTo(models.Author);
        Song.belongsToMany(models.Album, {through: 'AlbumSongs'});
    };
    return Song;
  };