const models = require('./../database/models');

//TODO: search by arbitrary parameters
getAllSongs = async (req, res, next) => {
    try {
        const songs = await models.Song.findAll({
            include: [
                {
                    model: models.Author,
                    as: 'Author'
                },
                {
                    model: models.Album,
                    as: 'Albums'
                }
            ]
        });
        return res.status(200).json({ songs });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
}

getSongById = async (req, res, next) => {
    try {
        const { songId } = req.params;
        const song = await models.Song.findOne({
            where: { id: songId },
            include: [
                {
                    model: models.Author,
                    as: 'Author'
                },
                {
                    model: models.Album,
                    as: 'Albums'
                }
            ]
        });
        if (song) {
            return res.status(200).json({ song });
        }
        return res.status(404).send('Song with specified ID does not exist.');
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
}

createSong = async (req, res, next) => {
    try {
        const song = await models.Song.create(req.body);
        return res.status(201).json({ song });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

deleteSong = async (req, res, next) => {
    try {
        const song = await models.Song.create(req.body);
        return res.status(201).json({ song });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

updateSong = async (req, res, next) => {
    try {
        const { songId } = req.params;
        const [updated] = await models.Song.update(req.body, {
            where: { id: songId }
        });
        if (updated) {
            const updatedSong = await models.Song.findOne({ where: { id: songId } });
            return res.status(200).json({ song: updatedSong });
        }
        return res.status(404).send('Song with specified ID does not exist.');

    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

module.exports = {
    getAllSongs,
    getSongById,
    createSong,
    deleteSong,
    updateSong
}