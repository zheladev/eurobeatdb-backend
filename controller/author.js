const models = require('./../database/models');

//TODO: search by arbitrary parameters
getAllAuthors = async (req, res, next) => {
    try {
        const authors = await models.Author.findAll({
            include: [
                {
                    model: models.Song,
                    as: 'Songs'
                }
            ]
        });
        return res.status(200).json({ authors });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
}

getAuthorById = async (req, res, next) => {
    try {
        const { authorId } = req.params;
        const author = await models.Author.findOne({
            where: { id: authorId },
            include: [
                {
                    model: models.Song,
                    as: 'Songs'
                }
            ]
        });
        if (author) {
            return res.status(200).json({ author });
        }
        return res.status(404).send('Author with specified ID does not exist.');
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
}

createAuthor = async (req, res, next) => {
    try {
        const author = await models.Author.create(req.body);
        return res.status(201).json({ author });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

deleteAuthor = async (req, res, next) => {
    try {
        const author = await models.Author.create(req.body);
        return res.status(201).json({ author });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

updateAuthor = async (req, res, next) => {
    try {
        const { authorId } = req.params;
        const [updated] = await models.Author.update(req.body, {
            where: { id: authorId }
        });
        if (updated) {
            const updatedAuthor = await models.Author.findOne({ where: { id: authorId } });
            return res.status(200).json({ author: updatedAuthor });
        }
        return res.status(404).send('Author with specified ID does not exist.');

    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

module.exports = {
    getAllAuthors,
    getAuthorById,
    createAuthor,
    deleteAuthor,
    updateAuthor
}